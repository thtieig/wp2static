#!/bin/bash

# THIS IS REQUIRED, replace the quoted values with the correct information.
APIKEY="<API_KEY>"
USERNAME="<ACCOUNT_USERNAME>"
REGION="lon"
CONTAINERNAME="<NAME>"
SITE="www.mydomain.com"

DEST=/tmp/static

# Clean up destination directory
ls $DEST > /dev/null 2&>1 && rm -rf $DEST || mkdir -p $DEST

# Create static copy of your site
httrack $SITE -w -O "$DEST" -q -%i0 -I0 -K0 -X -*/wp-admin/* -*/wp-login*

# Upload the static copy into your container via local network SNET (-I)
turbolift -u $USERNAME -a $APIKEY --os-rax-auth $REGION -I upload -s $DEST/$SITE -c $CONTAINERNAME


